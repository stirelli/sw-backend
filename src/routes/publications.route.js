const express = require('express');
const router = express.Router();
const publicationPaths = require('../constants/publication.paths');
const publicationController = require('../controllers/publication');

router.get(publicationPaths.request, publicationController.getPublications);

router.post(publicationPaths.create,
    publicationController.createPublication);

router.post(publicationPaths.update,
    publicationController.updatePublication);

router.post(publicationPaths.delete,
    publicationController.deletePublication);

module.exports = router;
