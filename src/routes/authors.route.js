const express = require('express');
const router = express.Router();
const authorsPaths = require('../constants/authors.paths');
const authorController = require('../controllers/author');

router.get(authorsPaths.request, authorController.getAuthors);

router.post(authorsPaths.create,
    authorController.createAuthor);

router.post(authorsPaths.update,
    authorController.updateAuthor);

router.post(authorsPaths.delete,
    authorController.deleteAuthor);

module.exports = router;
