const responseService= require('../services/response.service');
const authorService = require('../services/author.service');

const AuthorRequest = Object.freeze({
    Id: 'id',
    FirstName: 'firstname',
    LastName: 'lastname',
    Email: 'email',
    DateOfBirth: 'dateOfBirth'
});

const HttpStatusCodes = require('../constants/http-status-codes.enum');

const getAuthors = async (req, res, next) => {
    try {
        const result = await authorService.getAuthors();
        const response = responseService.ProcessResponse(result);
        return res.status(HttpStatusCodes.OK).json(response);
    } catch (ex) {
        console.error(ex);
        return next(ex);
    }
};

const createAuthor = async (req, res, next) => {
    try {
        const firstName = req.body[AuthorRequest.FirstName];
        const lastName = req.body[AuthorRequest.LastName];
        const email = req.body[AuthorRequest.Email];
        const dateOfBirth = req.body[AuthorRequest.DateOfBirth];

        const result = await authorService.createAuthor(
            firstName, lastName, email, dateOfBirth);
        const response = responseService.ProcessResponse(result);
        return res.status(HttpStatusCodes.OK).json(response);
    } catch (ex) {
        console.error(ex);
        return next(ex);
    }
};

const updateAuthor = async (req, res, next) => {
    try {
        const firstName = req.body[AuthorRequest.FirstName];
        const lastName = req.body[AuthorRequest.LastName];
        const email = req.body[AuthorRequest.Email];
        const dateOfBirth = req.body[AuthorRequest.DateOfBirth];

        const result = await authorService.updateAuthor(
            firstName, lastName, email, dateOfBirth);
        const response = responseService.ProcessResponse(result);
        return res.status(HttpStatusCodes.OK).json(response);
    } catch (ex) {
        console.error(ex);
        return next(ex);
    }
};

const deleteAuthor = async (req, res, next) => {
    try {
        const id = req.body[AuthorRequest.Id];
        const result = await authorService.deleteAuthor(id);
        const response = responseService.ProcessResponse(result);
        return res.status(HttpStatusCodes.OK).json(response);
    } catch (ex) {
        console.error(ex);
        return next(ex);
    }
};

module.exports = {
    getAuthors,
    createAuthor,
    updateAuthor,
    deleteAuthor
};
