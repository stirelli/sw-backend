const responseService= require('../services/response.service');
const publicationService = require('../services/publication.service');

const PublicationRequest = Object.freeze({
    Id: 'id',
    Title: 'title',
    Body: 'body',
    AuthorId: 'authorId',
    Datetime: 'datetime'
});

const HttpStatusCodes = require('../constants/http-status-codes.enum');

const getPublications = async (req, res, next) => {
    try {
        const result = await publicationService.getPublications();
        const response = responseService.ProcessResponse(result);
        return res.status(HttpStatusCodes.OK).json(response);
    } catch (ex) {
        console.error(ex);
        return next(ex);
    }
};

const createPublication = async (req, res, next) => {
    try {
        const title = req.body[PublicationRequest.Title];
        const body = req.body[PublicationRequest.Body];
        const authorId = req.body[PublicationRequest.AuthorId];
        const datetime = req.body[PublicationRequest.Datetime];

        const result = await publicationService
            .createPublication(title, body, authorId, datetime);
        const response = responseService.ProcessResponse(result);
        return res.status(HttpStatusCodes.OK).json(response);
    } catch (ex) {
        console.error(ex);
        return next(ex);
    }
};

const updatePublication = async (req, res, next) => {
    try {
        const id = req.body[PublicationRequest.Id];
        const title = req.body[PublicationRequest.Title];
        const body = req.body[PublicationRequest.Body];
        const authorId = req.body[PublicationRequest.AuthorId];
        const datetime = req.body[PublicationRequest.Datetime];

        const result = await publicationService
            .updatePublication(id, title, body, authorId, datetime);
        const response = responseService.ProcessResponse(result);
        return res.status(HttpStatusCodes.OK).json(response);
    } catch (ex) {
        console.error(ex);
        return next(ex);
    }
};

const deletePublication = async (req, res, next) => {
    try {
        const id = req.body[PublicationRequest.Id];
        const result = await publicationService.deletePublication(id);
        const response = responseService.ProcessResponse(result);
        return res.status(HttpStatusCodes.OK).json(response);
    } catch (ex) {
        console.error(ex);
        return next(ex);
    }
};

module.exports = {
    getPublications,
    createPublication,
    updatePublication,
    deletePublication
};
