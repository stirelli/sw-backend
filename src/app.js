const express = require('express');
const cors = require('cors');
const logger = require('morgan');
const parser = require('body-parser');
const authorRoutes = require('./routes/authors.route');
const publicationRoutes = require('./routes/publications.route');

const base = '/api';
const app = express();

app.use(cors());
app.use(parser.json());
app.use(logger('development'));
app.use(base, authorRoutes);
app.use(base, publicationRoutes);

module.exports = app;
