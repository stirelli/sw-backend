const port = process.env.APP_PORT || 3000;
const app = require('./app');

const startServer = async () => {
    try {
        app.listen(port, () => {
            console.log('Server listening on port ' + port);
            console.log(process.env);
        });
    } catch (ex) {
        console.error('Init database error: ', ex);
    }
};

(startServer)();
