const uuid = require('uuid/v4');
const dynamoDbClient = require('./dynamodb');
const TABLE_NAME = process.env.PUBLICATIONS_TABLE;

const query = async () => {
    const params = {
        TableName: TABLE_NAME
    };

    try {
        const result = await dynamoDbClient.getAll(params);
        return result.Items;
    } catch (ex) {
        return ex;
    }
};

const byId = async (id) => {
    const params = {
        TableName: TABLE_NAME,
        Key: { 'id': id }
    };

    try {
        return await dynamoDbClient.get(params);
    } catch (ex) {
        return ex;
    }
};

const byAuthorId = async (authorId) => {
    const params = {
        TableName: TABLE_NAME,
        Key: { 'authorId': authorId }
    };

    try {
        return await dynamoDbClient.get(params);
    } catch (ex) {
        return ex;
    }
};

const create = async (publication) => {
    const newId = uuid();
    const params = {
        TableName: TABLE_NAME,
        Item: {
            'id': newId,
            'title': publication.title,
            'body': publication.body,
            'authorId': publication.authorId,
            'datetime': publication.datetime
        }
    };

    try {
        await dynamoDbClient.create(params);
        return Object.assign({}, {id: newId}, publication);
    } catch (ex) {
        return ex;
    }
};

const update = async (id, publication) => {
    const update = 'set #a = :title, #b = :body, #c = ' +
        ':authorId, #d = :datetime';
    const params = {
        TableName: TABLE_NAME,
        Key: { id },
        UpdateExpression: update,
        ExpressionAttributeNames: {
            '#a': 'title',
            '#b': 'body',
            '#c': 'authorId',
            '#d': 'datetime'
        },
        ExpressionAttributeValues: {
            ':title': publication.title,
            ':body': publication.body,
            ':authorId': publication.authorId,
            ':datetime': publication.datetime
        }
    };

    try {
        return await dynamoDbClient.update(params);
    } catch (ex) {
        return ex;
    }
};

const remove = async (id) => {
    const params = {
        TableName: TABLE_NAME,
        Key: { id }
    };

    try {
        return await dynamoDbClient.remove(params);
    } catch (ex) {
        return ex;
    }
};

module.exports = {
    query,
    byId,
    byAuthorId,
    create,
    update,
    remove
};
