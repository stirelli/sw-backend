const AWS = require('aws-sdk');

class DynamoDbClient {
    constructor() {
        this.dbClient = this.getDynamoDbClient();
    }

    getDynamoDbClient() {
        const IS_OFFLINE = process.env.IS_OFFLINE;
        if (IS_OFFLINE === 'true') {
            return new AWS.DynamoDB.DocumentClient({
                region: 'localhost',
                endpoint: 'http://localhost:8000'
            })
        } else {
            return new AWS.DynamoDB.DocumentClient();
        };
    }

    getAll(params) {
        return new Promise((resolve, reject) => {
            this.dbClient.scan(params, (err, result) => {
                return err ? reject(err) : resolve(result);
            });
        });
    }

    get(params) {
        return new Promise((resolve, reject) => {
            this.dbClient.get(params, (err, result) => {
                return err ? reject(err) : resolve(result);
            });
        });
    }

    create(params) {
        return new Promise((resolve, reject) => {
            this.dbClient.put(params, (err) => {
                return err ? reject(err) : resolve();
            });
        });
    }

    update(params) {
        return new Promise((resolve, reject) => {
            this.dbClient.update(params, (err) => {
                return err ? reject(err) : resolve();
            });
        });
    }

    remove(params) {
        return new Promise((resolve, reject) => {
            this.dbClient.delete(params, (err) => {
                return err ? reject(err) : resolve();
            });
        });
    }
}

const instance = new DynamoDbClient();
module.exports = instance;
