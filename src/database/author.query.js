const uuid = require('uuid/v4');
const dynamoDbClient = require('./dynamodb');
const TABLE_NAME = process.env.AUTHORS_TABLE;

const query = async () => {
    const params = {
        TableName: TABLE_NAME,
    };

    try {
        const result = await dynamoDbClient.getAll(params);
        return result.Items;
    } catch (ex) {
        return ex;
    }
};

const byId = async (id) => {
    const params = {
        TableName: TABLE_NAME,
        Key: { 'id': id }
    };

    try {
        return await dynamoDbClient.get(params);
    } catch (ex) {
        return ex;
    }
};

const create = async (author) => {
    const newId = uuid();
    const params = {
        TableName: TABLE_NAME,
        Item: {
            'id': newId,
            'firstName': author.firstName,
            'lastName': author.lastName,
            'email': author.email,
            'dateOfBirth': author.dateOfBirth
        }
    };

    try {
        await dynamoDbClient.create(params);
        return Object.assign({}, {id: newId}, author);
    } catch (ex) {
        return ex;
    }
};

const update = async (id, author) => {
    const update = 'set #a = :firstName, #b = ' +
        ':lastName, #c = :email, #d = :dof';
    const params = {
        TableName: TABLE_NAME,
        Key: { 'id': id },
        UpdateExpression: update,
        ExpressionAttributeNames: {
            '#a': 'firstName',
            '#b': 'lastName',
            '#c': 'email',
            '#d': 'dof'
        },
        ExpressionAttributeValues: {
            ':firstName': author.firstName,
            ':lastName': author.lastName,
            ':email': author.email,
            ':dof': author.dof
        }
    };

    try {
        return await dynamoDbClient.update(params);
    } catch (ex) {
        return ex;
    }
};

const remove = async (id) => {
    const params = {
        TableName: TABLE_NAME,
        Key: { 'id': id }
    };

    try {
        return await dynamoDbClient.remove(params);
    } catch (ex) {
        return ex;
    }
};

module.exports = {
    query,
    byId,
    create,
    update,
    remove
};
