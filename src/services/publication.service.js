const publicationQuery = require('../database/publication.query');
const authorQuery = require('../database/author.query');
const utils = require('../utils/utils');
const errorMessages = require('../constants/publications-error-messages');

const authorExists = async (authorId) => {
    const author = await authorQuery.byId(authorId);
    return !utils.isObjectEmpty(author);
};

const publicationExists = async (publicationId) => {
    const publication = await publicationQuery.byId(publicationId);
    return !utils.isObjectEmpty(publication);
};

const getPublications = async () => {
    try {
        const authors = await authorQuery.query();
        const data = await publicationQuery.query();

        data.forEach((publication) => {
            const author = authors.find(
                _author => _author.id === publication.authorId
            );
            if (!author) {
                return;
            }
            publication.authorName =
                `${ author.firstName } ${ author.lastName}, ${ author.email}`;
        });

        return { data };
    } catch(ex) {
        return { data: {}, error: ex };
    }
};

const createPublication = async (title, body, authorId, datetime) => {
    const authorIsValid = await authorExists(authorId);
    if (!authorIsValid) {
        return { data: {}, error: errorMessages.invalidAuthor(authorId) };
    }

    const publication = { title, body, authorId, datetime };

    try {
        const createdPublication = await publicationQuery.create(publication);
        return { data: createdPublication };
    } catch (ex) {
        return { data: {}, error: ex };
    }
};

const updatePublication = async (id, title, body, authorId, datetime) => {
    const publicationIsValid = await publicationExists(id);
    if (!publicationIsValid) {
        return { data: {}, error: errorMessages.invalidPublicationId(id) };
    }

    const authorIsValid = await authorExists(authorId);
    if (!authorIsValid) {
        return { data: {}, error: errorMessages.invalidAuthor(authorId) };
    }

    const updatedPublication = { title, body, authorId, datetime, id };

    try {
        await publicationQuery.update(id, updatedPublication);
        return { updatedPublication };
    } catch (ex) {
        return { data: {}, error: ex };
    }
};

const deletePublication = async (id) => {
    try {
        const publicationIsValid = await publicationExists(id);
        if (!publicationIsValid) {
            return { data: {}, error: errorMessages.invalidPublicationId(id) };
        }

        await publicationQuery.remove(id);
        const data = { deletedPublicationId: id };
        return { data };
    } catch (ex) {
        return { data: {}, error: ex };
    }
};

module.exports = {
    getPublications,
    createPublication,
    updatePublication,
    deletePublication
};
