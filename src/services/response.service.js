class ResponseService {
    static ProcessResponse(result) {
        if (result.error) {
            return ResponseService.ResponseError([result.error])
        } else {
            return ResponseService.ResponseSucceed(result.data);
        }
    }

    static ResponseSucceed(data) {
        return {
            succeed: true,
            errorMessages: [],
            data
        };
    }

    static ResponseError(errorMessages) {
        return {
            succeed: false,
            errorMessages,
            data: {}
        };
    }
}

module.exports = ResponseService;
