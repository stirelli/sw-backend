const authorQuery = require('../database/author.query');
const utils = require('../utils/utils');
const errorMessages = require('../constants/authors-error-messages');

const authorExists = async (authorId) => {
    const author = await authorQuery.byId(authorId);
    return !utils.isObjectEmpty(author);
};

const getAuthors = async () => {
    try {
        const data = await authorQuery.query();
        return { data };
    } catch (ex) {
        return { data: {}, error: ex };
    }
};

const createAuthor = async (firstName, lastName, email, dateOfBirth) => {
    const author = { firstName, lastName, email, dateOfBirth };

    try {
        const createdAuthor = await authorQuery.create(author);
        return { data: createdAuthor };
    } catch (ex) {
        return { data: {}, error: ex };
    }
};

const updateAuthor = async (id, firstName, lastName, email, dof) => {
    const authorIsValid = await authorExists(id);
    if (!authorIsValid) {
        return { data: {}, error: errorMessages.invalidAuthorId(id) };
    }

    const updatedAuthor = { firstName, lastName, email, dof, id };

    try {
        await authorQuery.update(id, updatedAuthor);
        return { updatedAuthor };
    } catch (ex) {
        return { data: {}, error: ex };
    }
};

const deleteAuthor = async (id) => {
    try {
        const authorIsValid = await authorExists(id);
        if (!authorIsValid) {
            return { data: {}, error: errorMessages.invalidAuthorId(id) };
        }

        await authorQuery.remove(id);
        const data = { deletedAuthorId: id };
        return { data };
    } catch (ex) {
        return { data: {}, error: ex };
    }
};

module.exports = {
    getAuthors,
    createAuthor,
    updateAuthor,
    deleteAuthor
};
