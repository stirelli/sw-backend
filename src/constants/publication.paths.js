const publicationsPaths = Object.freeze({
    create: '/publications/new',
    request: '/publications',
    update: '/publications/edit',
    delete: '/publications/remove'
});

module.exports = publicationsPaths;
