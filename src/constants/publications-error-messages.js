const publicationErrorMessages = {
  invalidPublicationId: (pubId) => {
    return `Publication ${ pubId || '' } is not valid`;
  },
  invalidAuthor: (authorId) => {
    return `Author ${ authorId || '' } is not valid`;
  }
};

module.exports = publicationErrorMessages;
