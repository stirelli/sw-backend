const authorsPaths = Object.freeze({
    create: '/authors/new',
    request: '/authors',
    update: '/authors/edit',
    delete: '/authors/remove'
});

module.exports = authorsPaths;
