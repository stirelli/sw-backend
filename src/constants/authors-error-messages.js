const authorErrorMessages = {
  invalidAuthorId: (authorId) => {
    return `Author id ${ authorId || '' } is not valid`;
  }
};

module.exports = authorErrorMessages;
