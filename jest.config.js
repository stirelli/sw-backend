module.exports = {
  clearMocks: true,
  moduleFileExtensions: ['js'],
  testEnvironment: 'node',
  testMatch: ['**/tests/*.+(js)']
};
