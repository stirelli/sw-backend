const request = require('supertest');
const app = require('../src/app');
const HttpCodes = require('../src/constants/http-status-codes.enum');
const authorsPaths = require('../src/constants/authors.paths');
const base = '/api';

const author = {
    firstName: 'FirstName Test',
    lastName: 'LastName Test',
    email: 'test@gmail.com',
    dateOfBirth: '12/07/1992'
};

const updatedAuthor = {
    firstName: 'FirstName Test2',
    lastName: 'LastName Test2',
    email: 'test2@gmail.com',
    dateOfBirth: '12/03/1995'
};

describe('Create Author', () => {
    const url = base + authorsPaths.create;

    it('Should succeed', async () => {
        const body = {
            firstName: author.firstName,
            lastName: author.lastName,
            email: author.email,
            dateOfBirth: author.dateOfBirth
        };
        const response = await request(app).post(url).send(body);

        expect(response.status).toEqual(HttpCodes.OK);
        expect(response.body.succeed).toBeTruthy();
        updatedAuthor.id = response.body.data.id;
    });
});

describe('Update Author', () => {
    const url = base + authorsPaths.update;

    it('Should succeed', async () => {
        const body = {
            id: updatedAuthor.id,
            firstName: updatedAuthor.firstName,
            lastName: updatedAuthor.lastName,
            email: updatedAuthor.email,
            dateOfBirth: updatedAuthor.dateOfBirth
        };
        const response = await request(app).post(url).send(body);

        expect(response.status).toEqual(HttpCodes.OK);
        expect(response.body.succeed).toBeTruthy();
    });
});

describe('Delete Author', () => {
    const url = base + authorsPaths.delete;

    it('Should succeed', async () => {
        const body = { id: updatedAuthor.id };
        const response = await request(app).post(url).send(body);

        expect(response.status).toEqual(HttpCodes.OK);
        expect(response.body.succeed).toBeTruthy();
    });
});
