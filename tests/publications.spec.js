const request = require('supertest');
const app = require('../src/app');
const HttpCodes = require('../src/constants/http-status-codes.enum');
const publicationsPaths = require('../src/constants/publication.paths');
const authorsPaths = require('../src/constants/authors.paths');
const base = '/api';

const publicationTest = {
    title: 'Title Test',
    body: 'Body Test',
    datetime: '1566832262000'
};

const author = {
    firstName: 'FirstName Test',
    lastName: 'LastName Test',
    email: 'test@gmail.com',
    dateOfBirth: '12/07/1992'
};

const updatedPublicationTest = {
    title: 'Title Test2',
    body: 'Body Test2',
    datetime: '1566832262000'
};

describe('Create Publication', () => {
    const publicationUrl = base + publicationsPaths.create;
    const authorUrl = base + authorsPaths.create;

    it('Should succeed', async () => {
        const bodyAuthor = {
            firstName: author.firstName,
            lastName: author.lastName,
            email: author.email,
            dateOfBirth: author.dateOfBirth
        };
        const responseAuthor = await request(app).post(authorUrl)
            .send(bodyAuthor);

        const bodyPublication = {
            title: publicationTest.title,
            body: publicationTest.body,
            authorId: responseAuthor.body.data.id,
            datetime: publicationTest.datetime
        };
        const response = await request(app).post(publicationUrl)
            .send(bodyPublication);

        expect(response.status).toEqual(HttpCodes.OK);
        expect(response.body.succeed).toBeTruthy();
        publicationTest.id = response.body.data.id;
    });
});

describe('Update Publication', () => {
    const publicationUrl = base + publicationsPaths.create;
    const authorUrl = base + authorsPaths.create;

    it('Should succeed', async () => {
        const bodyAuthor = {
            firstName: author.firstName,
            lastName: author.lastName,
            email: author.email,
            dateOfBirth: author.dateOfBirth
        };
        const responseAuthor = await request(app).post(authorUrl)
            .send(bodyAuthor);

        const bodyPublication = {
            title: updatedPublicationTest.title,
            body: updatedPublicationTest.body,
            authorId: responseAuthor.body.data.id,
            datetime: updatedPublicationTest.datetime
        };
        const response = await request(app).post(publicationUrl)
            .send(bodyPublication);

        expect(response.status).toEqual(HttpCodes.OK);
        expect(response.body.succeed).toBeTruthy();
    });
});

describe('Delete Publication', () => {
    const publicationUrl = base + publicationsPaths.create;
    const authorUrl = base + authorsPaths.create;

    it('Should Succeed', async () => {
        const bodyAuthor = {
            firstName: author.firstName,
            lastName: author.lastName,
            email: author.email,
            dateOfBirth: author.dateOfBirth
        };
        const responseAuthor = await request(app).post(authorUrl)
            .send(bodyAuthor);

        const body = { id: responseAuthor.body.data.id };
        const response = await request(app).post(publicationUrl).send(body);

        expect(response.status).toEqual(HttpCodes.OK);
        expect(response.body.succeed).toBeTruthy();
    });
});
